﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.Odbc;

namespace obtenerNombreXml
{
    class Program
    {
        static void Main(string[] args)
        {
            //string dir = "SunatXML";
            string dir = @"E:\Prog\data";

            List<string> lista = new List<string>();
            foreach (string cadaArchivo in Directory.GetFiles(dir, "*.xml"))
            {
                string nombre = Path.GetFileName(cadaArchivo);
                if (nombre.EndsWith("-ori.xml"))
                {
                    //Console.WriteLine(nombre);
                    lista.Add(nombre.Substring(0, 30));
                }
            }

            //foreach (var item in lista)
            //{
            //    Console.WriteLine(item);
            //}

            List<string> lista2 = new List<string>();
            foreach (string cadaArchivo in Directory.GetFiles(dir, "*.xml"))
            {
                string nombre = Path.GetFileName(cadaArchivo);
                if (!nombre.EndsWith("-ori.xml"))
                {
                    if (!lista.Contains(nombre.Substring(0, 30)))
                    {
                        lista2.Add(nombre.Substring(14, 16));
                        //Console.WriteLine(nombre.Substring(14, 16));
                    }
                }
            }


            string name = "", user = "", pwd = "", server = "", db = "";

            GetVars(ref name, ref user, ref pwd, ref server, ref db);

            //string cx = "Driver={SQL Server};" + string.Format("Name={0};UID={1};PWD={2};Server={3};Database={4};", name, user, pwd, server, db);
            //Console.WriteLine(cx);
            var updquery = "update cpe_doc_cab set estadoregistro = 'L', encustodia = 0, ensunat = 0 where tipodocumento = '{0}' and serienumero = '{1}'";
            OdbcConnection cn = null;
            try
            {
                cn = GetConnectionFact(name, user, pwd, server, db);

                foreach(var item in lista2)
                {
                    //Console.WriteLine(item);
                    var td = item.Substring(0,2);
                    var sn = item.Substring(3);
                    //Console.WriteLine("td={0}   sn={1}", td, sn);
                    var query = string.Format(updquery, td, sn);
                    Console.WriteLine(query);
                    var commupd = new OdbcCommand(query, cn);
                    commupd.ExecuteNonQuery();

                }

                //foreach (var doc in lista)
                //{
                //    var unico = string.Format("{0}-{1}-{2}", doc.DocumentoCliente, doc.TipoDocumento, doc.SerieNumero);
                //    Console.WriteLine("Fecha:{0}: {1}", doc.FechaEmision, unico);
                //    var existe = CheckIfFileExistsOnServer("pdf/" + unico + ".pdf");
                //    if (!existe)
                //        Console.WriteLine("\t-->No Existe!");
                //    else
                //    {
                //        Console.WriteLine("\t-->Update 'S' en cpe_doc_cab");
                //        var commupd = new OdbcCommand(string.Format(updquery, doc.TipoDocumento, doc.SerieNumero), cn);
                //        commupd.ExecuteNonQuery();
                //    }
                //}

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (cn != null)
                    cn.Close();
            }

            Console.WriteLine("Presione ENTER ...");
            Console.ReadLine();
        }


        //    Console.WriteLine("Presione ENTER ...");
        //    Console.ReadLine();
        //}


        private static OdbcConnection GetConnectionFact(string name, string user, string pwd, string server, string db)
        {
            OdbcConnection cn;
            string cx = "Driver={SQL Server};" + string.Format("Name={0};UID={1};PWD={2};Server={3};Database={4};", name, user, pwd, server, db);
            Console.WriteLine(cx);
            cn = new OdbcConnection(cx);
            //cn = new OdbcConnection(string.Format("dsn={0};UID={1};PWD={2};", name, user, pwd));
            cn.Open();
            return cn;
        }

        private static string GetVars(ref string name, ref string user, ref string pwd, ref string server, ref string db)
        {

            var sr = new StreamReader("config.xml", Encoding.Default);
            var config = sr.ReadToEnd();
            var interno = GetByTag(ref config, "ODBC");
            name = GetByTag(ref interno, "NAME");
            user = GetByTag(ref interno, "USER");
            pwd = GetByTag(ref interno, "PWD");
            server = GetByTag(ref interno, "SERVER");
            db = GetByTag(ref interno, "DB");


            return "";

            //cmd = new OdbcCommand(MyString, cn);

            //cn.Open();
            //MessageBox.Show("Connected");

            //cn.Close();
        }

        private static string GetByTag(ref string texto, string tag)
        {
            //var tag = "ODBC";
            var tag1 = "<" + tag + ">";
            var tag2 = "</" + tag + ">";
            var pos = texto.IndexOf(tag1);
            if (pos >= 0)
            {
                var posini = pos + tag1.Length;
                var pos2 = texto.IndexOf(tag2);
                return texto.Substring(posini, pos2 - posini);
            }
            return "";
        }
    


    }
}
